import pygame, time
import frame, button, label, inputManager, textHolderUIs, utils
from pygame.locals import *

screenSize = (1440, 900)

pygame.init()

screen = pygame.display.set_mode(screenSize)

def fieldHandler(text):
    print repr(text)

f = frame.Frame("test", Rect((0,0) + screenSize))
b1 = button.Button("One", None, (150,35), (10,10))
b2 = button.Button("Two", None, (150,35), (10,60), True)
b3 = button.Button("Three", None, (150, 35), (10,110), enabled=False)
b4 = button.Button("Four", None, (150, 35), (10,160), enabled=False)
l1 = label.Label("Foo!", (200, 450))
f.addObject(b1)
f.addObject(b2)
f.addObject(b3)
f.addObject(b4)
f.addObject(l1)
f.assignMovementNodeVertical(b1, b2)
f.assignMovementNodeVertical(b2, b3)
f.assignMovementNodeVertical(b3, b4)
f.assignMovementNodeVertical(b4, b1)
f.currentHighlight = b4

KB = inputManager.KeyboardManager()
WM = inputManager.WindowManager()
MM = inputManager.MouseManager()
IM = inputManager.InputManager(KB, MM, WM)
KB.registerKeys(f.handleKey,
                (K_UP, K_DOWN, K_LEFT, K_RIGHT, K_RETURN),
                True, False)
MM.registerClick(f, [1, 2, 3, 4, 5, 6, 7, 8, 9], None)
MM.registerMovement(f)

keepGoing = True

def quitHandler():
    global keepGoing
    keepGoing = False
    print "Quit Handler!"

WM.registerQuit(quitHandler)

baseRect = Rect(500, 250, 600, 400)
pixelRect = utils.pixelBorderedRect(baseRect, 10)
percentRect = utils.percentBorderedRect(pixelRect, 0.05)
testImage = pygame.image.load("test_image.jpg")
testImage = testImage.convert()
blitPos, scaledImage = utils.fitImageInRect(testImage, percentRect)

while keepGoing:
    IM.tick()
    
    screen.fill((0x20,0x20,0x20))
    f.draw(screen)
    # test rect calculators
    pygame.draw.rect(screen, (0xA0, 0x10, 0x10), baseRect)
    pygame.draw.rect(screen, (0x10, 0xA0, 0x10), pixelRect)
    pygame.draw.rect(screen, (0x10, 0x10, 0xA0), percentRect)
    screen.blit(scaledImage, blitPos)
    pygame.display.flip()
    time.sleep(0.01)
