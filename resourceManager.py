import os, pygame
import os.path as path
from pygame.locals import *

class ResourceManager:
    def __init__(self, resourcesRoot=None):
        self.rootDir = resourcesRoot
        self.images = {}
    
    def getImage(self, imageName):
        if imageName not in self.images:
            imagePath = path.join(self.rootDir, imageName)
            image = pygame.image.load(imagePath)
            self.images[imageName] = image
        return self.images[imageName]
