import string
import utils
from pygame.locals import *

printable = tuple(string.digits + string.letters + string.punctuation + " \t")

class TextField:
    def __init__(self, text="", cursorPosition=0, scrollPosition=0,
                 displayWidth=None, enterCallback=None, font=None):
        self.text = text
        self.cursorPosition = cursorPosition
        self.scrollPosition = scrollPosition # scroll point of displayed text
        self.displayWidth = displayWidth # used to determine when to scroll
        self.enterCallback = enterCallback
        self.forceCursorSanity()
        self.font = font
    
    def handleKey(self, key, downP, activeMods):
        # Input is handled here instead of TextFieldUI
        #  so that different UIs can be wrapped around it
        if activeMods == None:
            activeMods = []
        
        if downP != True:
            return
        
        if key == K_RETURN:
            self.enterCallback(self.text)
        elif key in (K_LEFT, K_RIGHT):
            direction = 1 if (key == K_RIGHT) else -1
            self.moveCursor(direction)
        elif key in (K_UP, K_DOWN):
            val = 0 if (key == K_UP) else -1
            self.setCursorPosition(val)
        elif utils.keyOrdPrintable(key):
            if KMOD_SHIFT in activeMods:
                char = string.capitalize(chr(key))
            else:
                char = chr(key)
            self.insertString(char)
    
    def setScroll(self):
        self.forceCursorSanity()
        if self.displayWidth == None: # No max width, we don't need to scroll
            self.scrollPosition = 0
            return
        if self.cursorPosition < self.scrollPosition:
            # Cursor beyond the left edge of the view
            self.scrollPosition = self.cursorPosition
        elif self.cursorPosition >= (self.scrollPosition + self.displayWidth):
            # Cursor beyond the right edge of the view
            self.scrollPosition = self.cursorPosition - (self.displayWidth/2)
    
    def deleteString(self, number):
        pre, post = self.splitTextByCursor()
        if number >= 0: # forward delete
            post = post[number:]
        else: # backspace
            pre = pre[:number]
            self.cursorPosition += number # number negative, therefore +
        self.text = pre + post
        self.setScroll()
    
    def insertString(self, text):
        pre, post = self.splitTextByCursor()
        pre += text
        self.text = pre + post
        self.cursorPosition += len(text)
        self.setScroll()
    
    def setCursorPosition(self, position):
        if position < 0:
            maxCursor = len(self.text) + 1 # +1 here because -0 doesn't exist
            pos = maxCursor + position # position negative, therefore +
            self.cursorPosition = pos
        else:
            self.cursorPosition = position
        self.forceCursorSanity()
    
    def moveCursor(self, direction):
        saneDir = -1 if direction < 0 else 1
        self.cursorPosition += saneDir
        self.setScroll() # also sanifies cursor position
    
    def forceCursorSanity(self):
        self.cursorPosition = max(0, self.cursorPosition)
        self.cursorPosition = min(len(self.text), self.cursorPosition)
    
    def splitTextByCursor(self):
        pre = self.text[:self.cursorPosition]
        post = self.text[self.cursorPosition:]
        return pre, post
    
    def __repr__(self):
        template = "TextField(%s, %s, %s, %s, %s)"
        data = (repr(self.text), repr(self.cursorPosition),
                repr(self.scrollPosition), repr(self.displayWidth),
                repr(self.enterHandler))
        rendered = template % data
        return rendered

class TextBox:
    def __init__(self):
        self.lines = []
        self.cursorLine = 0
        self.cursorColumn = 0
        self.scrollPosition = 0
        self.displayWidth = None

