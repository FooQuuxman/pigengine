import pygame, string, time, sys
import utils
from pygame.locals import *

testLines = ["To be, or not to be; that is the question. Whether tis nobler to suffer the slings and arrows of great misfortune. Or to take arms against a sea of troubles.",
             "^1The quick brown fox jumps over the lazy dog",
             "^2Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
             "^3We impose order on the chaos of organic evolution. You exist because we allow it, and you will end because we demand it.",
             "",
             "^9The Forces of the Universe bend to Me."]


colorTable = {"1":Color(0xFF0000FF), "2":Color(0xFF8000FF),
              "3":Color(0xFFFF00FF), "4":Color(0x00FF00FF),
              "5":Color(0x00FFFFFF), "6":Color(0x0000FFFF),
              "7":Color(0xFF00FFFF), "8":Color(0xFFFFFFFF),
              "9":Color(0x606060FF), "0":Color(0xB0B0B0FF)}

validCharacters = " " + string.punctuation + string.letters + string.digits

renderTextChunk = utils.renderTextChunk
renderTextLine = utils.renderTextLine

def getColorizedLineByWidth(line, width):
    # split line into lines with color codes
    allLines = [] # need more than one line if it exceeds the width
    currentLine = [] # each line may contain different colors
    currentChunk = [] # temp space while building each chunk
    currentFg = colorTable["8"] # white
    
    index = 0
    widthCounter = 0
    max = len(line)
    escape = False # escape things this way to get bounds checking for free
    while index < max:
        currentChar = line[index]
        if escape == True:
            if currentChar in string.digits: # escaping a color code
                # hit a chunk boundary, store the previous one
                chunk = ("".join(currentChunk), currentFg)
                currentLine.append(chunk)
                
                currentChunk = []
                currentFg = colorTable[currentChar]
                
            elif currentChar == "^": # escaping the escape character
                currentChunk.append(currentChar)
            escape = False
        else:
            if currentChar == "^": # escaping next char, ignore this
                escape = True
                index += 1 # index++ here because we need to skip rest of loop
                continue
            
            else: # normal
                currentChunk.append(currentChar)
                widthCounter += 1
        if widthCounter >= width: # hit the width limit, break the line
            widthCounter = 0
            chunk = ("".join(currentChunk), currentFg)
            currentLine.append(chunk)
            allLines.append(tuple(currentLine))
            currentLine = []
            currentChunk = []
        index += 1 # all paths lead to the next index
    if len(currentChunk) > 0:
        chunk = ("".join(currentChunk), currentFg)
        currentLine.append(chunk)
        allLines.append(tuple(currentLine))
    return allLines

class Console:
    # Defaults
    fontName = "Monospace"
    fontSize = 16
    prompt = ":> "
    keyRepeat = (200, 50)
    def __init__(self, closeKey, commandHandler):
        self.closeKey = closeKey # Key that will close the console
        self.cmdHandler = commandHandler # call this function for each execution
        self.lines = []
        self.currentCommand = ""
        self.cursorPosition = 0
        self.cursorScrollPosition = 0
        self.recentCommands = []
        self.commandListPosition = 0
        self.scroll = 0
        self.active = False # is the console being displayed
        # timer used to control how fast the console slides up and down
        self.activeTimerSet = 0 
        self.activeTimer = 0
        self.activeMoving = False
        # We change the repeat when open, need change it back afterwards
        self.oldKeyRepeat = None
        # Set from defaults
        self.setFont()

    def getCommandLine(self):
        cmdLine = Console.prompt + self.currentCommand[0:self.cursorPosition]
        cursorString = "^" + self.currentCommand[self.cursorPosition]
    
    def commandEditPoint(self):
        pre = self.currentCommand[:self.cursorPosition]
        post = self.currentCommand[self.cursorPosition:]
        return (pre, post)
    
    def inputChar(self, character):
        pre, post = self.commandEditPoint()
        pre += character
        self.currentCommand = pre + post
        self.cursorPosition += 1
    
    def deleteChar(self, backP=True):
        pre, post = self.commandEditPoint()
        if backP == True:
            pre = pre[:-1] # backspace
            self.cursorPosition -= 1
            self.forceCursorSanity()
        else:
            post = post[1:] # forawrd delete
        self.currentCommand = pre + post
        
    def forceCursorSanity(self):
        maxLength = len(self.currentCommand)
        self.cursorPosition = max(min(self.cursorPosition, maxLength), 0)

    def addLine(self, line):
        self.lines.append(line)
    
    def enterLine(self):
        if len(self.currentCommand) == 0:
            return
        self.cmdHandler(self.currentCommand)
        self.addLine(self.currentCommand) # TODO: callback function
        self.currentCommand = ""
        self.forceCursorSanity()
    
    def toggleActive(self):
        self.active = not self.active
        self.activeTimer = time.time()
        self.activeMoving = True
        if self.active == True:
            self.oldKeyRepeat = pygame.key.get_repeat()
            pygame.key.set_repeat(*Console.keyRepeat)
        else:
            pygame.key.set_repeat(*self.oldKeyRepeat)
        print pygame.key.get_repeat()
    
    def moveCursor(self, direction):
        if direction not in (1, -1):
            return
        self.cursorPosition += direction
        self.forceCursorSanity()
    
    def tick(self, eventList):
        # unlike most tick() functions this doesn't care about the time
        # eventList is expected to only contain KEYDOWN events (change?)
        for event in eventList:
            key = event.key
            char = str(event.unicode)
            if key in (K_BACKSPACE, K_DELETE):
                back = True if (key == K_BACKSPACE) else False
                self.deleteChar(back)
            elif key in (K_UP, K_DOWN):
                pass
            elif key in (K_LEFT, K_RIGHT):
                direction = 1 if (key == K_RIGHT) else -1
                self.moveCursor(direction)
            elif key in (K_LSHIFT, K_RSHIFT):
                pass # Ignore this
            elif key == K_RETURN:
                self.enterLine()
            elif char in validCharacters:
                print char
                self.inputChar(char)
        
        elapsed = time.time() - self.activeTimer
        if self.activeMoving and (elapsed >= self.activeTimerSet):
                self.activeMoving = False
    
    def getTextForSize(self, size):
        """Returns list of lines as they should be on the console, index 0 at the top, index -1 is the prompt"""
        width = size[0]
        height = size[1]
        
        scrollback = self.scroll
        displayLines = [] # the lines that fit on the screen, i=0 is the bottom
        
        index = len(self.lines) - 1 # need to step backwards through the list
        while (index >= 0) and (len(displayLines) < (height - 1)):
            currentLine = self.lines[index]
            lineParts = getColorizedLineByWidth(currentLine, width)
            lineParts.reverse()
            for part in lineParts:
                displayLines.insert(0, part)
            index -= 1
        # pad the top of the list with blank lines to simplify drawing
        for i in range((height - 1) - len(displayLines)):
            blankLine = (("", colorTable["8"]),)
            displayLines.insert(0, blankLine)
        # add command line
        return displayLines
        
    def setFont(self, fontName=None, fontSize=None):
        name = fontName if fontName != None else Console.fontName
        size = fontSize if fontSize != None else Console.fontSize
        match = pygame.font.match_font(name, False, False)
        self.font = pygame.font.Font(match, size)
    
    def renderPrompt(self):
        white = (255, 255, 255)
        black = (0, 0, 0)
        pre, post = self.commandEditPoint()
        renderedText = []
        # render everything before the cursor
        text = Console.prompt + pre
        renderedText.append(self.font.render(text, True, white, black))
        lineWidth, lineHeight = renderedText[-1].get_size()
        
        # render the cursor
        if len(post) == 0:
            post = " "
        cursor = post[0]
        post = post[1:]
        renderedText.append(self.font.render(cursor, True, black, white))
        lineWidth += renderedText[-1].get_width()
        
        # render everything after the cursor
        if len(post) != 0:
            renderedText.append(self.font.render(post, True, white, black))
            lineWidth += renderedText[-1].get_width()
        
        # piece it together
        lineSurf = pygame.Surface((lineWidth, lineHeight))
        currentWidth = 0
        for chunk in renderedText:
            chunkSize = chunk.get_size()
            blitRect = (currentWidth, 0) + chunkSize
            currentWidth += chunkSize[0]
            lineSurf.blit(chunk, blitRect)
        
        return lineSurf
    
    def drawConsoleText(self, consoleSurf):
        charSize = self.font.size("M")
        
        w, h = consoleSurf.get_size()
        cCH = int(h / charSize[1])
        cCW = int(w / charSize[0])
        consoleSizeInChars = (cCW, cCH)
        
        drawX = 0
        drawY = 0
        
        wht = colorTable["8"]
        blk = colorTable["0"]
        consoleLines = self.getTextForSize(consoleSizeInChars)
        
        renderedLines = []
        for line in consoleLines:
            rLine = renderTextLine(self.font, line, True)
            renderedLines.append(rLine)
        
        currentHeight = 0
        for line in renderedLines:
            lineSize = line.get_size()
            lineRect = (0, currentHeight) + lineSize
            consoleSurf.blit(line, lineRect)
            currentHeight += charSize[1] # step down line by line
        
        promptSurf = self.renderPrompt()
        lineRect = (0, currentHeight) + promptSurf.get_size()
        consoleSurf.blit(promptSurf, lineRect)
    
    def drawConsole(self, screen, sizeFraction):
        if (not self.active) and (not self.activeMoving):
            return
        
        if self.activeMoving and (self.activeTimerSet > 0):
            elapsed = time.time() - self.activeTimer
            openFraction = elapsed / self.activeTimerSet
            if self.active == False:
                openFraction = 1 - openFraction
        else:
            openFraction = 1.0
        
        scrSize = screen.get_size()
        consoleHeight = int(scrSize[1] * sizeFraction * openFraction)
        consoleRect = (0, 0, scrSize[0], consoleHeight)
        
        
        conSurf = pygame.Surface((consoleRect[2], consoleRect[3]))
        
        conSurf.fill(Color(0x000000FF), consoleRect)
        
        self.drawConsoleText(conSurf)
        
        borderStart = (0, consoleHeight-1)
        borderEnd = (scrSize[0], consoleHeight-1)
        pygame.draw.line(conSurf, Color(0xFF0000FF), borderStart, borderEnd)
        
        screen.blit(conSurf, consoleRect)

def run_demo_mode():
    pygame.init()
    
    def cmdFunc(cmd):
        print "Command:", repr(cmd)
        pieces = cmd.split(None, 1)
        command = pieces[0]
        args = pieces[1:]
        print "\t", command
        print "\t", args
        if command in conFuncs.keys():
            conFuncs[command](args)
    
    console = Console(K_BACKQUOTE, cmdFunc)
    console.activeTimerSet = 0.2
    console.lines = list(testLines)
    
    conVars = {"screenSize":(1280, 800),
               "bgColor":[0x11, 0x11, 0xaa]}
    
    conFuncs = {"set":(lambda v: conVars.__setitem__(v.split(None, 1)[0], eval(v.split(None, 1)[1])))}
    
    displayFlags = RESIZABLE | DOUBLEBUF
    scr = pygame.display.set_mode(conVars["screenSize"], displayFlags)
    
    tickTime = time.time()
    
    print "Key repeat:", pygame.key.get_repeat()
    
    keepGoing = True
    while keepGoing:
        keys = []
        for event in pygame.event.get():
            if event.type == QUIT:
                keepGoing = False
            elif event.type == KEYDOWN:
                if event.key == console.closeKey:
                    console.toggleActive()
                    print "switching console:", console.active
                elif console.active:
                    keys.append(event)
            elif event.type == VIDEORESIZE:
                conVars["screenSize"] = event.size
                pygame.display.set_mode(conVars["screenSize"], displayFlags)
        console.tick(keys)
        
        scr.fill(conVars["bgColor"])
        
        console.drawConsole(scr, 0.4)
        
        pygame.display.flip()
        time.sleep(0.01)

if __name__ == "__main__":
    run_demo_mode()
