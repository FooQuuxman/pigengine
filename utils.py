import pygame, string
from pygame.locals import *


colorTable = {"normalObject":  Color(0x1010FFFF),
              "normalText":    Color(0xFFFF00FF),
              "highlight":     Color(0xD0D000FF),
              "disabledObject":Color(0x444444FF),
              "disabledText":  Color(0xA0A0A0FF)}

def getColor(colorName):
    """getColor(colorName)
    get colors from the color table by name\n"""
    return colorTable[colorName]

def keyOrdPrintable(ordinal):
    """keyOrdPrintable(ordinal) ==> bool(is the ordinal a printable char?)\n"""
    c = chr(ordinal)
    return c in string.printable

def renderTextChunk(font, chunk, antialias):
    """renderTextChunk(font, chunk, antialias)
    font is the font object used to render the text
    chunk is a tuple or list in the format (text, fgColor)
    antialias is a bool\n"""
    text, fgColor = chunk
    renderedText = font.render(text, antialias, fgColor, Color(0x00000000))
    return renderedText

def renderTextLine(font, line, antialias):
    """renderTextLine(font, line, antialias)
    font is the font object used to render the text
    line is a list of chunks as described in renderTextChunk()
    antialias is a bool\n"""
    renderedChunks = []
    width = 0
    for chunk in line:
        renderedChunk = renderTextChunk(font, chunk, antialias)
        width += renderedChunk.get_width()
        renderedChunks.append(renderedChunk)
    height = renderedChunks[0].get_height()
    lineSize = (int(width), int(height))
    lineSurf = pygame.Surface(lineSize) # size of the entire line
    blitWidth = 0
    for chunk in renderedChunks:
        chunkWidth = chunk.get_width()
        lineSurf.blit(chunk, (blitWidth, 0, chunkWidth, height))
        blitWidth += chunkWidth
    return lineSurf

def percentBorderedRect(rect, percentage):
    x, y = rect.topleft
    w, h = rect.size
    xPercent = w * percentage
    yPercent = h * percentage
    x += xPercent
    y += yPercent
    w -= (xPercent * 2)
    h -= (yPercent * 2)
    return Rect(x, y, w, h)

def pixelBorderedRect(rect, pixels):
    x, y = rect.topleft
    w, h = rect.size
    if (pixels * 2) > w:
        return None
    elif (pixels * 2) > h:
        return None
    x += pixels
    y += pixels
    w -= (pixels * 2)
    h -= (pixels * 2)
    return Rect(x, y, w, h)

def centerRectInRect(largeRect, smallRect):
    print "=== centerRectInRect ==="
    print largeRect, smallRect
    halfSmall = smallRect.center
    halfLarge = largeRect.center
    print halfSmall
    x = halfLarge[0] - halfSmall[0]
    y = halfLarge[1] - halfSmall[1]
    drawRect = Rect((x, y), smallRect.size)
    print drawRect
    return drawRect

def fitImageInRect(image, rect):
    print "=== fitImageInRect ==="
    iSize = image.get_size()
    print iSize
    rSize = rect.size
    xScale = float(iSize[0]) / float(rSize[0])
    yScale = float(iSize[1]) / float(rSize[1])
    tooBig = (iSize[0] > rSize[0]) or (iSize[1] > rSize[1])
    tooSmall = (iSize[0] < rSize[0]) and (iSize[1] < rSize[1])
    if tooBig or tooSmall:
        print tooBig, tooSmall
        if xScale > yScale:
            newX = rSize[0]
            newY = int(iSize[1] / xScale)
        else:
            newX = int(iSize[0] / yScale)
            newY = rSize[1]
        print (newX, newY)
        scaledImage = pygame.transform.smoothscale(image, (newX, newY))
        return scaledImage
    else: # none larger, at least one equal, no scaling necessary
        print "goldilocks"
        return image
