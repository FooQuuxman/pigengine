from textHolders import *
from pygame.locals import *

callbackReturn = None

def testCallback(value):
    global callbackReturn
    callbackReturn = value

def test_TF_init():
    # Test default __init__
    field = TextField()
    assert field.text == ""
    assert field.cursorPosition == 0
    assert field.scrollPosition == 0
    assert field.displayWidth == None
    assert field.enterCallback == None
    # Test __init__ with init values
    cb = (lambda : testCallback("Flax!"))
    field = TextField("foo bar baz", 3, 1, 40, cb)
    assert field.text == "foo bar baz"
    assert field.cursorPosition == 3
    assert field.scrollPosition == 1
    assert field.displayWidth == 40
    assert field.enterCallback == cb
    return

def test_TF_setScroll():
    field = TextField("Foobarbazquux!", 3)
    # Test with no displayWidth
    field.setScroll()
    assert (field.scrollPosition == 0)
    # Test inside the width
    field.displayWidth = 10
    field.cursorPosition = 5
    field.setScroll()
    assert (field.scrollPosition == 0)
    # Test right of the view
    field.cursorPosition = 11
    field.setScroll()
    assert (field.scrollPosition == 6)
    # Test left of the view
    field.cursorPosition = 1
    field.setScroll()
    assert (field.scrollPosition == 1)
    return

def test_TF_moveCursor():
    field = TextField()
    # Test empty text, moving left
    field.moveCursor(-1)
    assert (field.cursorPosition == 0)
    # Test empty text, moving right
    field.moveCursor(1)
    assert (field.cursorPosition == 0)
    # Test middle of text, moving right
    field.text = "foobarbaz"
    field.cursorPosition = 5
    field.moveCursor(1)
    assert (field.cursorPosition == 6)
    # Test middle of text, moving left
    field.moveCursor(-1)
    assert (field.cursorPosition == 5)
    return

def test_TF_forceCursorSanity():
    field = TextField("foo bar baz", 5)
    # Test inside bounds
    field.forceCursorSanity()
    assert field.cursorPosition == 5
    # Test higher than bounds
    field.cursorPosition = 25
    field.forceCursorSanity()
    assert field.cursorPosition == 11
    # Test lower than bounds
    field.cursorPosition = -3
    field.forceCursorSanity()
    assert field.cursorPosition == 0
    # Test zero length string
    field.text = ""
    field.cursorPosition = 2
    field.forceCursorSanity()
    assert field.cursorPosition == 0
    return

def test_TF_splitTextByCursor():
    field = TextField("abcdefghi")
    # Test split at beginning of line
    pre, post = field.splitTextByCursor()
    assert (pre == "") and (post == "abcdefghi")
    # Test split in middle of line
    field.cursorPosition = 4
    pre, post = field.splitTextByCursor()
    assert (pre == "abcd") and (post == "efghi")
    # Test split at end of line
    field.cursorPosition = 9
    pre, post = field.splitTextByCursor()
    assert (pre == "abcdefghi") and (post == "")
    return

def test_TF_deleteString():
    field = TextField("foo bar baz", 5)
    # Test fwd-delete not at the end
    field.deleteString(3) # fwd-delete 3 chars
    assert field.text == "foo bbaz"
    # Test backspace not at the end
    field.deleteString(-2) # backspace 2 chars
    assert field.text == "foobaz"
    # Test fwd-delete overshooting the end
    field.deleteString(50) # fwd-delete ALL THE THINGS
    assert field.text == "foo"
    # Test backspace overshooting the end
    field.cursorPosition -= 1
    field.deleteString(-50) # backspace ALL THE THINGS
    assert field.text == "o"
    return

def test_TF_insertString():
    field = TextField()
    # Test adding to zero length string
    field.insertString("foo")
    assert (field.text == "foo") and (field.cursorPosition == 3)
    # Test adding to end of string
    field.insertString(" bar")
    assert (field.text == "foo bar") and (field.cursorPosition == 7)
    # Test adding to beginning of string
    field.cursorPosition = 0
    field.insertString("baz ")
    assert (field.text == "baz foo bar") and (field.cursorPosition == 4)
    # Test adding to middle of string
    field.insertString("quux ")
    assert (field.text == "baz quux foo bar") and (field.cursorPosition == 9)
    return

def test_TF_setCursorPosition():
    field = TextField()
    # === Empty Text ===
    # Test position 0
    field.setCursorPosition(0)
    assert (field.cursorPosition == 0)
    # Test positive position
    field.setCursorPosition(4)
    assert (field.cursorPosition == 0)
    # Test negative position
    field.setCursorPosition(-2)
    assert (field.cursorPosition == 0)
    # === Filled Text ===
    # Test positive position < max
    field.text = "Jabber jabber jabber!"
    field.setCursorPosition(15)
    assert (field.cursorPosition == 15)
    # Test positive position > max
    field.setCursorPosition(50)
    assert (field.cursorPosition == 21)
    # Test negative position > min
    field.setCursorPosition(-10)
    assert (field.cursorPosition == 12)
    # Test negative position < min
    field.setCursorPosition(-50)
    assert (field.cursorPosition == 0)
    return

def test_TF_handleKey():
    field = TextField()
    field.enterCallback = testCallback
    # === Empty Test control chars ===
    # Test enter
    global callbackReturn
    callbackReturn = "Foo!"
    field.handleKey(K_RETURN, True, None)
    assert (callbackReturn == "")
    # Test left arrow
    field.handleKey(K_LEFT, True, None)
    assert (field.cursorPosition == 0)
    # Test right arrow
    field.handleKey(K_RIGHT, True, None)
    assert (field.cursorPosition == 0)
    # Test up arrow
    field.handleKey(K_UP, True, None)
    assert (field.cursorPosition == 0)
    # Test down arrow
    field.handleKey(K_DOWN, True, None)
    assert (field.cursorPosition == 0)
    # === Test printables ===
    # Test Keydown
    field.handleKey(K_a, True, None)
    assert ((field.cursorPosition == 1) and (field.text == "a"))
    # Test Keyup
    field.handleKey(K_a, False, None)
    assert ((field.cursorPosition == 1) and (field.text == "a"))
    # Test symbol
    field.handleKey(K_COMMA, True, None)
    assert ((field.cursorPosition == 2) and (field.text == "a,"))
    # Test shift letter
    field.handleKey(K_b, True, (KMOD_SHIFT,))
    assert ((field.cursorPosition == 3) and (field.text == "a,B"))
    # Test shift symbol
    field.handleKey(K_DOLLAR, True, (KMOD_SHIFT,))
    assert ((field.cursorPosition == 4) and (field.text == "a,B$"))
    # === Test control chars not empty ===
    # Test enter
    field.handleKey(K_RETURN, True, None)
    assert (callbackReturn == "a,B$")
    # Test left arrow
    field.handleKey(K_LEFT, True, None)
    assert (field.cursorPosition == 3)
    # Test up arrow
    field.handleKey(K_UP, True, None)
    assert (field.cursorPosition == 0)
    # Test right arrow
    field.handleKey(K_RIGHT, True, None)
    assert (field.cursorPosition == 1)
    # Test down arroe
    field.handleKey(K_DOWN, True, None)
    assert (field.cursorPosition == 4)
    return

def test_TextField():
    print "testing TextField"
    print "\ttesting __init__"
    test_TF_init()
    
    print "\ttesting forceCursorSanity"
    test_TF_forceCursorSanity()
    
    print "\ttesting splitTextByCursor"
    test_TF_splitTextByCursor()
    
    print "\ttesting setScroll"
    test_TF_setScroll()
    
    print "\ttesting setCursorPosition"
    test_TF_setCursorPosition()
    
    print "\ttesting moveCursor"
    test_TF_moveCursor()
    
    print "\ttesting deleteString"
    test_TF_deleteString()
    
    print "\ttesting insertString"
    test_TF_insertString()
    
    print "\ttesting handleKey"
    test_TF_handleKey()
    
    print "TextField test complete"

def runTests():
    test_TextField()
