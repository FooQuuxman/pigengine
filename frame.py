import pygame, utils, cmath
from pygame.locals import *

dirTable = {K_UP:0, K_DOWN:1, K_LEFT:2, K_RIGHT:3}

class Frame:
    def __init__(self, name, rect):
        self.name = name
        self.mouseRect = rect
        self.contents = []
        self.activeContents = []
        self.currentHighlight = None
        self.inputCapture = None
        self.movementNet = {} # <obj> : (up, down, left, right)
    
    def addObject(self, obj):
        self.contents.append(obj)
        obj.setFrame(self)
        self.movementNet[obj] = [None, None, None, None]
        if obj.activeObject == True:
            self.activeContents.append(obj)
    
    def assignMovementNodeHorizontal(self, left, right):
        self.movementNet[left][3] = right
        self.movementNet[right][2] = left
    
    def assignMovementNodeVertical(self, up, down):
        self.movementNet[up][1] = down
        self.movementNet[down][0] = up
    
    def draw(self, surface):
        self.drawBackground(surface)
        for obj in self.contents:
            hi = bool(obj == self.currentHighlight)
            obj.draw(surface, hi)
    
    def drawBackground(self, surface):
        surface.fill(Color(0x202060FF))
    
    def mouseMotion(self, pos):
        for obj in self.activeContents:
            if obj.mouseRect.collidepoint(pos) == True:
                self.currentHighlight = obj
    
    def mouseClick(self, pos, downP, button):
        for obj in self.activeContents:
            if obj.mouseRect.collidepoint(pos) == True:
                obj.mouseClick(pos, downP, button)
    
    def getMovementNode(self, obj, direction):
        return self.movementNet[obj][direction]
    
    def moveHighlight(self, direction):
        current = self.currentHighlight
        node = self.getMovementNode(current, direction)
        if node != None:
            self.currentHighlight = node
    
    def handleKey(self, key, downP, mods):
        if self.inputCapture == None:
            if downP == True:
                if key in (K_UP, K_DOWN, K_LEFT, K_RIGHT):
                    obj = self.currentHighlight
                    node = self.getMovementNode(obj, dirTable[key])
                    if node != None:
                        self.currentHighlight = node
                elif key == K_RETURN:
                    if self.currentHighlight != None:
                        self.currentHighlight.activate()
