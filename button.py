import pygame, utils, pygame.font
from pygame.locals import *

class ButtonCore:
    def __init__(self, rect, callback, enabled):
        self.activeObject = True
        self.callback = callback
        self.enabled = enabled
        self.hilight = False
        self.rect = rect
    
    def mouseClick(self, position, downP, button):
        self.activate()
    
    def mouseEnter(self, position):
        self.hilight = True
    
    def mouseExit(self):
        self.hilight = False
    
    def activate(self):
        print "Button activated"
        if (self.enabled == True) and (self.callback != None):
            self.callback()
    
    def draw(self):
        assert False, "draw() not yet implemented"

class LabelButton(ButtonCore):
    def __init__(self, title, rect, callback, enabled, fontSize=25):
        ButtonCore.__init__(self, rect, callback, enabled)
        self.title = title
        self.font = pygame.font.Font(None, fontSize)
    
    def render(self):
        base = pygame.Surface(self.rect.size)
        buttonColor = Color(0xFFFF00FF) if self.hilight else Color(0x0000FFFF)
        base.fill(buttonColor)
        text = self.font.render(self.title, True, Color(0xFFFFFFFF))
        # Calc draw position
        baseCenter = base.get_rect().center
        textCenter = text.get_rect().center
        x = baseCenter[0] - textCenter[0]
        y = baseCenter[1] - textCenter[1]
        base.blit(text, (x, y))
        return base
        
    def draw(self, screen):
        button = self.render()
        screen.blit(button, self.rect.topleft)

class Button:
    edgeWidth = 5
    nC  = utils.getColor("normalObject")
    nTC = utils.getColor("normalText")
    hC  = utils.getColor("highlight")
    dC  = utils.getColor("disabledObject")
    dTC = utils.getColor("disabledText")
    # (highlight, enabled) : (edge, body, text)
    colorTable = {(False, False):(dC, dC, dTC),
                  (False, True): (nC, nC, nTC),
                  (True, False): (hC, dC, dTC),
                  (True, True):  (hC, nC, nTC)}
    def __init__(self, title, callback, size, position,
                 enabled=True, customEdgeWidth=None):
        self.activeObject = True
        self.title = title
        self.callback = callback
        self.size = size
        self.position = position
        self.mouseRect = Rect(position + size)
        self.enabled = enabled
        if customEdgeWidth != None:
            self.edgeWidth = customEdgeWidth
        else:
            self.edgeWidth = Button.edgeWidth
        self.font = pygame.font.Font(None, 20)
        self.cached = None
        self.cacheCode = None
    
    def setFrame(self, frame):
        self.frame = frame
    
    def mouseClick(self, position, downP, button):
        print "mouseClick:", position, downP, button
        self.activate()
    
    def activate(self):
        if self.enabled == True:
            print "Button activate", repr(self.title)
            if self.callback != None:
                self.callback()
        else:
            print "Button activate: DISABLED", repr(self.title)
    
    def draw(self, surface, highlight=False):
        if self.cacheCode != (self.enabled, highlight):
            edgeC, bodyC, textC = Button.colorTable[(highlight, self.enabled)]
            mySurf = pygame.Surface(self.size)
            mySurf.fill(edgeC)
            eW = self.edgeWidth
            bodyRect = Rect(eW, eW,
                            self.size[0] - (eW*2),
                            self.size[1] - (eW*2))
            mySurf.fill(bodyC, bodyRect)
            # Label
            textSurf = self.font.render(self.title, True, textC)
            # position label in the center of the button
            tSize = textSurf.get_size()
            textDraw = list(bodyRect.center)
            textDraw[0] -= tSize[0] / 2
            textDraw[1] -= tSize[1] / 2
            mySurf.blit(textSurf, textDraw)
            self.cached = mySurf
            self.cacheCode = (self.enabled, highlight)
        surface.blit(self.cached, self.position)
