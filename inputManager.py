import pygame
from pygame.locals import *

def activeModKeyList():
    activeList = []
    allMods = pygame.key.get_mods()
    def test(modCode):
        if (allMods & modCode) == True:
            activeList.append(modCode)
    test(KMOD_LSHIFT)
    test(KMOD_RSHIFT)
    test(KMOD_SHIFT)
    test(KMOD_CAPS)
    test(KMOD_LCTRL)
    test(KMOD_RCTRL)
    test(KMOD_CTRL)
    test(KMOD_LALT)
    test(KMOD_RALT)
    test(KMOD_ALT)
    test(KMOD_LMETA)
    test(KMOD_RMETA)
    test(KMOD_META)
    test(KMOD_NUM)
    test(KMOD_MODE)

class EventTypeError(Exception): pass

KEYBOARD = 1
MOUSE = 2
JOYSTICK = 3

class InputManager:
    def __init__(self, keyboard=None, mouse=None, window=None):
        self.keyboardManager = keyboard
        self.mouseManager = mouse
        self.windowManager = window
        self.namedInputs = {} # (type, value) type = mouse, key, joy
    
    def registerNamedInput(self, name):
        self.namedInputs[name] = None
    
    def assignKeyToNamedInput(self, name, callback, key):
        if self.namedInputs.has_key(key) == False:
            raise KeyError
        self.namedInputs[name] = (KEYBOARD, key)
        self.keyboardManager.registerKeys(key, callback, True, True)
    
    def assignMouseToNamedInput(self, name, callback, button):
        if self.namedInputs.has_key(key) == False:
            raise KeyError
        self.namedInputs[name] = (MOUSE, button)
        self.mouseManager.registerClick()
    
    def tick(self):
        for event in pygame.event.get():
            if event.type in (QUIT, VIDEORESIZE):
                if self.windowManager != None:
                    self.windowManager.handleEvent(event)
            elif event.type in (KEYUP, KEYDOWN):
                if self.keyboardManager != None:
                    self.keyboardManager.handleEvent(event)
            elif event.type in (MOUSEMOTION, MOUSEBUTTONUP, MOUSEBUTTONDOWN):
                if self.mouseManager != None:
                    self.mouseManager.handleEvent(event)

class WindowManager:
    def __init__(self):
        self.quitCallback = None
        self.resizeCallback = None
    
    def registerQuit(self, callback):
        self.quitCallback = callback
    
    def handleEvent(self, event):
        if event.type == QUIT:
            if self.quitCallback != None:
                self.quitCallback()

class MouseManager:
    def __init__(self):
        self.clickObjectsDown = []
        self.clickObjectsUp = []
        self.movementObjects = []
        self.universalClickHandlers = {}
        self.universalMoveHandler = None
    
    def registerUniversalClick(self, button, callback):
        self.universalClickHandlers[button] = callback
    
    def registerClick(self, obj, buttonsDown=None, buttonsUp=None):
        if buttonsDown != None:
            self.clickObjectsDown.append((obj, buttonsDown))
        if buttonsUp != None:
            self.clickObjectsUp.append((obj, buttonsUp))
    
    def registerMovement(self, obj):
        self.movementObjects.append(obj)
    
    def handleEvent(self, event):
        if event.type in (MOUSEBUTTONUP, MOUSEBUTTONDOWN):
            downP = (event.type == MOUSEBUTTONDOWN)
            pos = event.pos
            button = event.button
            objList = self.clickObjectsDown if downP else self.clickObjectsUp
            for (obj, buttonList) in objList:
                if button not in buttonList:
                    continue
                rect = obj.mouseRect
                if rect.collidepoint(pos) == True:
                    obj.mouseClick(pos, downP, button)
                    return # Got a hit, we don't want to hit the universal
            if self.universalClickHandlers.has_key(button):
                self.universalClickHandlers[button](pos, downP)
        elif event.type == MOUSEMOTION:
            for obj in self.movementObjects:
                if obj.mouseRect.collidepoint(event.pos):
                    obj.mouseMotion(event.pos)
                    return # Got a hit, we don't want to hit the universal
            if self.universalMoveHandler != None:
                self.universalMoveHandler(event.pos)

class KeyboardManager:
    def __init__(self):
        self.keyDowns = {}
        self.keyUps = {}
    
    def registerKeys(self, callback, keys, keyDown=True, keyUp=True):
        if keyDown == True:
            for key in keys:
                self.keyDowns[key] = callback
        if keyUp == True:
            for key in keys:
                self.keyUps[key] = callback
    
    def handleEvent(self, event):
        if event.type == KEYDOWN:
            downP = True
            call = self.keyDowns.get(event.key)
        elif event.type == KEYUP:
            downP = False
            call = self.keyUps.get(event.key)
        else:
            raise EventTypeError("KeyboardManager requires keyboard events.")
        if call != None:
            activeMods = activeModKeyList()
            call(event.key, downP, activeMods)
