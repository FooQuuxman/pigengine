import pygame, utils
from pygame.locals import *

class Label:
    def __init__(self, text, position, fontSize=20,
                 color=None, background=None):
        self.activeObject = False
        self.fontSize = fontSize
        self.size = None
        self.position = position
        self.text = text
        self.color = color if color else utils.getColor("normalText")
        self.background = background
        self.font = pygame.font.Font(None, fontSize)
        self.frame = None
        self.rect = None
        self.cached = None
    
    def render(self):
        textColor = utils.getColor("normalText")
        self.cached = self.font.render(self.text, True, textColor)
        self.size = self.cached.get_size()
        self.rect = Rect(self.position + self.size)
    
    def setFrame(self, frame):
        self.frame = frame
    
    def draw(self, surface, highlight=False):
        if not self.cached:
            self.render()
        surface.blit(self.cached, self.rect)
