import pygame
import textHolders, utils
from pygame.locals import *

class TextFieldUI:
    def __init__(self, position, width, callback, enabled=True, fontSize=20):
        self.activeObject = True
        self.position = position
        self.width = width
        self.enabled = enabled
        self.textCore = textHolders.TextField(enterCallback=callback)
        self.frame = None
        # Text stuff
        self.fontSize = fontSize
        self.font = pygame.font.Font(None, self.fontSize)
        self.height = None
        self.cached = None
    
    def setFrame(self, frame):
        self.frame = frame
    
    def setInputLock(self, lockedP):
        # behave according to keyboard focus
        self.lockInput = lockedP
    
    def renderText(self):
        pass
    
    def drawBackground(self, surface):
        surface.fill(utils.getColor("disabledObject"))
        w, h = surface.get_size()
        w -= 4
        h -= 4
        r = Rect(2, 2, w, h)
        surface.fill(utils.getColor("normalObject"), r)
    
    def draw(self, surface, highlight=False):
        surf = pygame.Surface((self.width, self.height))
        self.drawBackground(surf)
        blitRect = Rect(self.position + self.width + self.height)
        surface.blit(surf, blitRect)
